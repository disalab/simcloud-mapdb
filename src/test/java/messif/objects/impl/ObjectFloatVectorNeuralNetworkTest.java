/*
 *  This file is part of SimCloud MapDB library: https://bitbucket.org/disalab/simcloud-mapdb
 *
 *  SimCloudMapDB library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloudMapDB library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloudMapDB library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.objects.impl;

import junit.framework.TestCase;

/**
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class ObjectFloatVectorNeuralNetworkTest extends TestCase {
    
    public ObjectFloatVectorNeuralNetworkTest(String testName) {
        super(testName);
    }

    public void testTest() {
        assertTrue(true);
    }

//
//    /**
//     * Test of getCompressedData method, of class ObjectFloatVectorNeuralNetwork.
//     */
//    private void getCompressedData() {
//        System.out.println("getCompressedData");
//        BitSet zeroesToInit = null;
//        ObjectFloatVectorNeuralNetworkFull instance = null;
//        float[] expResult = null;
//        float[] result = instance.getCompressedData(zeroesToInit);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    //public static final Random rand = ;
//    public static final float [] randomObject = getRandomArray(128, new Random());
//
//    private static float[] getRandomArray(int size, Random rand) {
//        float [] retVal = new float[size];
//        for (int i = 0; i < retVal.length; i++) {
//            retVal[i] = (rand.nextBoolean()) ? 0f : rand.nextFloat();
//        }
//        return retVal;
//    }
//
//    /**
//     * Test of getRawData method, of class ObjectFloatVectorNeuralNetwork.
//     */
//    public void testGetRawData_3args_1() {
//        System.out.println("getRawData1");
//
//        ObjectFloatVectorNeuralNetworkFullL2 obj = new ObjectFloatVectorNeuralNetworkFullL2(randomObject);
//        BitSet zeroes = new BitSet();
//        float[] compressedData = obj.getCompressedData(zeroes);
//
//        float [] rawData = obj.getRawData(128, zeroes, compressedData);
//
//        System.out.println("BitSet: " + Arrays.toString(rawData));
//    }
//
//    /**
//     * Test of getRawData method, of class ObjectFloatVectorNeuralNetwork.
//     */
//    public void testGetRawData_3args_2() {
//        System.out.println("getRawData2");
//        ObjectFloatVectorNeuralNetworkFullL2 obj = new ObjectFloatVectorNeuralNetworkFullL2(randomObject);
//        BitSet zeroes = new BitSet();
//        float[] compressedData = obj.getCompressedData(zeroes);
//
//        float [] rawData = obj.getRawData(128, zeroes.toLongArray(), compressedData);
//
//        System.out.println("long array: " + Arrays.toString(rawData));
//    }
}
