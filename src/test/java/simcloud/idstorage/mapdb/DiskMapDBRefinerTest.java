/*
 *  This file is part of SimCloud MapDB library: https://bitbucket.org/disalab/simcloud-mapdb
 *
 *  SimCloudMapDB library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloudMapDB library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloudMapDB library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.idstorage.mapdb;

import junit.framework.TestCase;
import messif.data.DataObject;
import messif.data.util.DataObjectIterator;
import messif.data.util.DataObjectList;
import messif.data.util.StreamDataObjectIterator;
import messif.distance.DataObjectDistanceFunc;
import messif.distance.impl.L2DistanceFloats;
import messif.operation.crud.InsertOperation;
import messif.record.DataObjectMapDBSerializer;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class DiskMapDBRefinerTest extends TestCase {
    
    public DiskMapDBRefinerTest(String testName) {
        super(testName);
    }
    
    protected FileMapDBRefiner mapDB;
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        File file = new File("/tmp/mapdbfile.bin");
        file.delete();
        mapDB = new FileMapDBRefiner(file.getAbsolutePath(), new DataObjectMapDBSerializer(), new DataObjectDistanceFunc<>("data", new L2DistanceFloats()));
        mapDB.init();
    }
    

    @Override
    protected void tearDown() throws Exception {
        super.tearDown(); //To change body of generated methods, choose Tools | Templates.
        try {
            mapDB.destroy();
        } catch (Throwable ex) {
            Logger.getLogger(DiskMapDBRefinerTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    public static DataObjectIterator getDataObjects() {
        return new StreamDataObjectIterator(new BufferedReader(
                new InputStreamReader(DiskMapDBRefinerTest.class.getResourceAsStream("/dataobjects.json"))));
    }


    /**
     * Test of storeObject method, of class MemoryMapDBRefiner.
     */
    public void testStoreAndReadObjects() throws Exception, Throwable {
        System.out.println("storeandReadObjects");
        final DataObjectList dataObjects = new DataObjectList(getDataObjects());

        mapDB.evaluate(InsertOperation.create(dataObjects.toArray(new DataObject[0])));
        //mapDB.storeObject(id, object);
        
//        assertEquals(expResult, result);

        // get the IDs
        Collection<String> ids = new ArrayList<>(dataObjects.size());
        for (DataObject dataObject : dataObjects) {
            ids.add(dataObject.getID());
        }

        final Iterator<DataObject> dataObjectIterator = mapDB.readObjects(ids);
        List<DataObject> answer = new ArrayList<>();
        dataObjectIterator.forEachRemaining(answer::add);

        assertEquals(dataObjects.size(), answer.size());
        
//        mapDB.destroy();
//        assertEquals(object.getLocatorURI(), readObject.getLocatorURI());
    }

    
//    /**
//     * Test of deleteObject method, of class MemoryMapDBRefiner.
//     */
//    public void testDeleteObject() {
//        System.out.println("deleteObject");
//        String id = "";
//        MemoryMapDBRefiner instance = new MemoryMapDBRefiner();
//        LocalAbstractObject expResult = null;
//        LocalAbstractObject result = instance.deleteObject(id);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

}
