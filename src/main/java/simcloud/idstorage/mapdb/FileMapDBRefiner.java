/*
 *  This file is part of SimCloud MapDB library: https://bitbucket.org/disalab/simcloud-mapdb
 *
 *  SimCloudMapDB library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloudMapDB library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloudMapDB library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.idstorage.mapdb;

 import messif.algorithm.Algorithm;
 import messif.data.DataObject;
 import messif.distance.DistanceFunc;
 import messif.utility.ExtendedProperties;
 import org.mapdb.DB;
 import org.mapdb.DBMaker;
 import org.mapdb.HTreeMap;
 import org.mapdb.Serializer;

 import java.io.File;
 import java.io.IOException;
 import java.util.HashMap;

/**
 * A simple implementation of the {@link simcloud.idstorage.StorageAndRefinerAlgorithm} that keeps the crud in a memory
 * {@link HashMap}.
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class FileMapDBRefiner extends AbstractMapDBRefiner {

    /**
     * Class serial id for serialization.
     */
    private static final long serialVersionUID = 8593703L;

    /**
     * Name of the MapDB file name with the disk database.
     */
    protected String dbFileName;

    /**
     * If true, the full memory mapped file is used when opening the database. 
     * Default: true.
     */
    protected boolean useFullMMapped;

    /**
     * If true, the default "one transaction" MapDB mode is used (using WAL).
     * Default: false.
     */
    protected boolean useWAL;
    
    /**
     * The MapDB database (constructed during deseralization and initialization).
     */
    protected transient DB db;

    /**
     * Disk-based hash map of the crud objects indexed by their string locators.
     */
    protected transient HTreeMap<String, DataObject> storage;

    /**
     * Creates the MapDB file storage algorithm given a name of the file with MapDB database and
     * MapDB serializer to store the crud objects (values). The database uses full memory mapped
     * file.
     *
     * @param dbFileName name of the file with MapDB database
     * @param valueSerializer MapDB serializer to store the crud objects (values)
     * @throws IllegalArgumentException
     */
    @AlgorithmConstructor(description = "Creates persistent MapDB storage", arguments = {"name of the DB file", "instance of the value serializer"})
    public FileMapDBRefiner(String dbFileName, Serializer valueSerializer, DistanceFunc<DataObject> defaultDistanceFunc) throws IllegalArgumentException {
        this(dbFileName, valueSerializer, true, false, defaultDistanceFunc);
    }

    /**
     * Creates the MapDB file storage algorithm given a name of the file with MapDB database and
     * MapDB serializer to store the crud objects (values).
     *
     * @param dbFileName name of the file with MapDB database
     * @param valueSerializer MapDB serializer to store the crud objects (values)
     * @param useFullMMapped if true, then full memory mapped file is used when opening the
     * database.
     * @param useWAL If true, the default "one transaction" MapDB mode is used (using WAL)
     * @throws IllegalArgumentException
     */
    @AlgorithmConstructor(description = "Creates persistent MapDB storage", arguments = {"name of the DB file", "instance of the value serializer", "flag, if the full memory mapped file should be used"})
    public FileMapDBRefiner(String dbFileName, Serializer valueSerializer, boolean useFullMMapped, boolean useWAL, DistanceFunc<DataObject> defaultDistanceFunc) throws IllegalArgumentException {
        this(dbFileName, valueSerializer, useFullMMapped, useWAL, defaultDistanceFunc, Algorithm.defaultConfiguration());
    }

    /**
     * Creates the MapDB file storage algorithm given a name of the file with MapDB database and
     * MapDB serializer to store the crud objects (values).
     *
     * @param dbFileName name of the file with MapDB database
     * @param valueSerializer MapDB serializer to store the crud objects (values)
     * @param useFullMMapped if true, then full memory mapped file is used when opening the
     * database.
     * @param useWAL If true, the default "one transaction" MapDB mode is used (using WAL)
     * @throws IllegalArgumentException
     */
    @AlgorithmConstructor(description = "Creates persistent MapDB storage", arguments = {"name of the DB file", "instance of the value serializer", "flag, if the full memory mapped file should be used"})
    public FileMapDBRefiner(String dbFileName, Serializer valueSerializer, boolean useFullMMapped, boolean useWAL, DistanceFunc<DataObject> defaultDistanceFunc, ExtendedProperties configuration) throws IllegalArgumentException {
        super("Disk MapDB store in file '" + dbFileName + "'", valueSerializer, defaultDistanceFunc, configuration);
        this.dbFileName = dbFileName;
        this.useFullMMapped = useFullMMapped;
        this.useWAL = useWAL;
        this.db = createMapDB();
        this.storage = createStorage();
    }

    /**
     * Deserialization of this object including creation of new MapDB and Map<String, Object>
     * storage.
     */
    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        this.db = createMapDB();        
        this.storage = createStorage();
    }

    /**
     * Creates the MapDB database object from the file name in the config of this object. The
     * transactions are disabled and it should be closed when the JVM ends.
     *
     * @return the newly created DB over the given file
     */
    private DB createMapDB() {
        DBMaker dbMaker = DBMaker.newFileDB(new File(dbFileName)).closeOnJvmShutdown();
        if (! useWAL) {
            dbMaker.transactionDisable();
        }
        if (useFullMMapped) {
            return dbMaker.mmapFileEnable().make();
        }
        return dbMaker.mmapFileEnablePartial().make();
    }

    /**
     * Creates a new map storage assuming that the MapDB database object already exists. If the hash
     * map with this name was already created in the DB, it is returned.
     *
     * @return the MapDB map from the MapDB database
     */
    private HTreeMap<String, DataObject> createStorage() {
        return db.createHashMap(getName()).keySerializer(Serializer.STRING).valueSerializer(valueSerializer).counterEnable().<String, DataObject>makeOrGet();
    }

    @Override
    public HTreeMap<String, DataObject> getStorage() {
        return storage;
    }
    
    @Override
    protected DB getMapDB() {
        return db;
    }
}
