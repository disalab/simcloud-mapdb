/*
 *  This file is part of SimCloud MapDB library: https://bitbucket.org/disalab/simcloud-mapdb
 *
 *  SimCloudMapDB library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloudMapDB library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloudMapDB library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.idstorage.mapdb;

 import messif.data.DataObject;
import messif.distance.DistanceFunc;
 import messif.utility.ExtendedProperties;
 import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;

import java.io.IOException;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class MemoryMapDBRefiner extends AbstractMapDBRefiner {

    /**
     * Class serial id for serialization.
     */
    private static final long serialVersionUID = 8593501L;

    /**
     * The MapDB database (constructed during deseralization and initialization).
     */
    protected transient DB db;

    /**
     * Memory hash map of the crud objects indexed by their string locators.
     */
    protected transient HTreeMap<String, DataObject> storage;

    /**
     * Creates the MapDB memory storage algorithm given MapDB serializer to store the crud objects
     *   (values) into memory.
     * @param valueSerializer MapDB serializer to store the crud objects (values)
     * @throws IllegalArgumentException 
     */
    public MemoryMapDBRefiner(Serializer valueSerializer, DistanceFunc<DataObject> defaultDistanceFunc, ExtendedProperties configuration) throws IllegalArgumentException {
        super("memory MapDB ID-object storage and refiner (DATA IS LOST AFTER SERIALIZATION)", valueSerializer, defaultDistanceFunc, configuration);
        this.db = createMapDB();
        this.storage = createStorage();
    }

    /**
     * Deserialization of this object including creation of new MapDB and Map<String, Object> storage.
     */
    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        this.db = createMapDB();
        this.storage = createStorage();
    }
    
        /**
     * Creates the MapDB database object from the file name in the config of this object. The transactions
     *  are disabled and it should be closed when the JVM ends.
     * @return the newly created DB over the given file
     */
    private HTreeMap<String, DataObject> createStorage() {
        return db.createHashMap(getName()).keySerializer(Serializer.STRING).valueSerializer(valueSerializer).counterEnable().<String, DataObject>makeOrGet();
    }

    private DB createMapDB() {
        //return DBMaker.newMemoryDirectDB().transactionDisable().closeOnJvmShutdown().make();
        return DBMaker.newHeapDB().transactionDisable().closeOnJvmShutdown().make();
    }

    @Override
    public HTreeMap<String, DataObject> getStorage() {
        return storage;
    }

    @Override
    protected DB getMapDB() {
        return db;
    }
}
