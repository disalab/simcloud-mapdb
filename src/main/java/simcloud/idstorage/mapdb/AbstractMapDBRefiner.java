/*
 *  This file is part of SimCloud MapDB library: https://bitbucket.org/disalab/simcloud-mapdb
 *
 *  SimCloudMapDB library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloudMapDB library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloudMapDB library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.idstorage.mapdb;


import messif.algorithm.Algorithm;
import messif.algorithm.executor.RunningOperationMonitor;
import messif.appl.Application;
import messif.bucket.BucketStorageException;
import messif.data.DataObject;
import messif.data.util.DataObjectList;
import messif.distance.DistanceFunc;
import messif.operation.answer.ListingAnswer;
import messif.operation.info.PrintAllObjectsOperation;
import messif.operation.info.answer.PrintAllObjectsAnswer;
import messif.operation.info.processor.PrintAllObjectsProcessorBase;
import messif.operation.processing.ProcessorConfiguration;
import messif.operation.processing.impl.OneStepProcessor;
import messif.operation.search.GetRandomObjectsOperation;
import messif.record.Record;
import messif.utility.ExtendedProperties;
import org.mapdb.DB;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;
import simcloud.filter.DataObjectCondition;
import simcloud.filter.SelectionCondition;
import simcloud.idstorage.StorageAndRefinerAlgorithm;

import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

 /**
 * An abstract implementation of the {@link StorageAndRefinerAlgorithm} that keeps crud in MapDB hash tree
 * map. Specific sub-classes create and specific MapDB collections (disk, memory, etc.); the storage
 *  is provided to this abstract class by getter method {@link #getStorage()}.
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public abstract class AbstractMapDBRefiner extends StorageAndRefinerAlgorithm implements Serializable {

    /**
     * Class serial id for serialization.
     */
    private static final long serialVersionUID = 8593402L;

    /**
     * Serializator of the values in the map store.
     */
    protected final Serializer valueSerializer;

    private transient Random random = new Random(System.currentTimeMillis   ());

    /**
     * Creates this MapDB refiner given a name of the {@link Algorithm#getName()}.
     * @param algName
     * @param mapDBSerializer
     * @throws IllegalArgumentException
     */
    public AbstractMapDBRefiner(String algName, Serializer mapDBSerializer, DistanceFunc<DataObject> defaultDistanceFunc, ExtendedProperties configuration) throws IllegalArgumentException {
        super(algName, defaultDistanceFunc, configuration);
        this.valueSerializer = mapDBSerializer;
    }

     @Override
     protected void registerProcessors() {
         super.registerProcessors();
         this.handler.register(ProcessorConfiguration.create(GetRandomObjectsOperation.class, ListingAnswer.class)
                 .withProcessor(RandomObjectsProcessor::new).build());
         this.handler.register(ProcessorConfiguration.create(PrintAllObjectsOperation.class, PrintAllObjectsAnswer.class)
                 .withProcessor((operation) -> {
                     try {
                         return new PrintAllObjectsProcessor(operation);
                     } catch (IOException e) {
                         throw new RuntimeException(e);
                     }
                 }).build());
     }


    /**
     * Deserialize and set the transient fields.
     */
    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        this.random = new Random(System.currentTimeMillis());
    }

    /**
     * This method is to be implemented by sub-classes; it returns specific MapDB map collection
     *  to be used for object operations.
     * @return 
     */
    public abstract HTreeMap<String, DataObject> getStorage();

    /**
     * This method is to be implemented by sub-classes; it returns specific MapDB map collection
     *  to be used for object operations.
     * @return 
     */
    protected abstract DB getMapDB();
    
    @Override
    public DataObject readObject(String id) {
        return getStorage().get(id);
    }

    @Override
    public Iterator<DataObject> readObjects(Collection<String> ids) {
        DataObjectList retVal = new DataObjectList(ids.size());
        for (String string : ids) {
            DataObject obj = getStorage().get(string);
            if (obj != null) {
                retVal.add(obj);
            } else {
                System.out.println("ID missing: " + string);
            }
        }
        return retVal.iterator();
    }

     @Override
     public boolean checkID(String id) {
         return getStorage().containsKey(id);
     }

     @Override
    public Collection<String> checkIDs(Collection<String> ids) {
        final HTreeMap<String, DataObject> storage = getStorage();
        Collection<String> retVal = ids.stream().filter(storage::containsKey).collect(Collectors.toList());
        return retVal;
    }

     @Override
    public boolean storeObject(String id, DataObject object) throws BucketStorageException {
        return getStorage().putIfAbsent(id, object) == null;
    }

    @Override
    public DataObject deleteObject(String id) {
        return getStorage().remove(id);
    }

    @Override
    public int getNumberOfObjects() {
        return getStorage().size();
    }


    // *********************     Operation processing   ************************ //

    /**
     * Process an operation to return given number of random objects.
     */
    protected class RandomObjectsProcessor extends OneStepProcessor<GetRandomObjectsOperation, ListingAnswer> {

        final ListingAnswer.Builder builder;

        /**
         * Creates this processor given the operation to be processed.
         *
         * @param operation operation to be executed
         */
        public RandomObjectsProcessor(GetRandomObjectsOperation operation) {
            super(operation);
            builder = new ListingAnswer.Builder(operation, ListingAnswer.class);
        }

        @Override
        protected void process() throws Exception {
            int number = operation.getObjectCount();
            if (getStorage().isEmpty() || number <= 0) {
                return;
            }

            Iterator<String> iterator = getStorage().keySet().iterator();//values().iterator();
            for (int i = 0; i < number; i++) {
                int skips = random.nextInt(getStorage().size() / number);

                int counter = 0;
                String nextID;
                do {
                    nextID = iterator.next();
                } while (iterator.hasNext() && counter++ < skips);
                builder.add(getStorage().get(nextID));
            }
        }

        @Override
        public ListingAnswer finish() {
            return builder.getAnswer();
        }
    }

    /**
     * Closes the MapDB storage; called by the finalize method of this algorithm.
     */
    protected void closeMapDB() {
        DB db = getMapDB();
        if (db != null && !db.isClosed()) {
            commit(db);
            db.close();
            Logger.getLogger(AbstractMapDBRefiner.class.getName()).log(Level.INFO, "closing the MapDB");
        }
    }
    
    /**
     * This method compacts the MapDB storage (especially the file storage). It can be called
     *  from the outside directly by {@link Application#methodExecute(java.io.PrintStream, java.lang.String...) }
     */
    public void compactMapDB() {
        DB db = getMapDB();
        if (db != null) {
            commit(db);
            db.compact();
            Logger.getLogger(AbstractMapDBRefiner.class.getName()).log(Level.INFO, "compacting the MapDB");
        }        
    }
    
    /**
     * This method commits the changes (updates) done in MapDB. It can be called
     *  from the outside directly by {@link Application#methodExecute(java.io.PrintStream, java.lang.String...) }
     */
    public void commitChanges() {
        DB db = getMapDB();
        if (db != null) {
            commit(db);
            Logger.getLogger(AbstractMapDBRefiner.class.getName()).log(Level.INFO, "commiting updates into MapDB");
        }
    }
    
    /**
     * Calls the MapDB commit in the single global transaction and then clears current update log in
     *  {@link Algorithm}. This is not ideal because we will loose operations that came after the commit
     *  started and before the WAL was deleted.
     * @param db 
     */
    protected void commit(DB db) {
        final RunningOperationMonitor runningOperationMonitor = getFirstExecutor(RunningOperationMonitor.class);
        // Acquire all locks, thus waiting for all currently running operations and disable additional
        if (runningOperationMonitor != null)
            runningOperationMonitor.stopUpdates();
        try {
            super.beforeStoreToFile(null);
            db.commit();
            super.afterStoreToFile(null, true);
        } finally {
            // Unlock operations
            if (runningOperationMonitor != null)
                runningOperationMonitor.continueUpdates();
        }
    }
    
    @Override
    public void destroy() throws Throwable {
        closeMapDB();
        super.destroy();
    }

    @Override
    @SuppressWarnings("FinalizeNotProtected")
    public void finalize() throws Throwable {
        closeMapDB();
        super.finalize(); //To change body of generated methods, choose Tools | Templates.
    }

//    /**
//     * A method which processes given operation by sequential scan on all objects in the storage.
//     * @param operation
//     * @return
//     * @throws CloneNotSupportedException
//     */
//    public boolean processExhaustively(RankingSingleQueryOperation operation) throws CloneNotSupportedException {
//        try {
//            operation.evaluate(new IteratorToAbstractObjectIterator<>(getStorage().values().iterator()));
//            return true;
//        } catch (Exception ex) {
//            Logger.getLogger(AbstractMapDBRefiner.class.getName()).warning(ex.toString());
//            return false;
//        }
//    }

     /**
      * Reading all objects (or IDs) from the storage and printing it to a file (and to the answer as well).
      */
     private class PrintAllObjectsProcessor extends PrintAllObjectsProcessorBase  {

         protected boolean successful = false;

         final SelectionCondition<DataObject> filterCondition;

         public PrintAllObjectsProcessor(PrintAllObjectsOperation operation) throws IOException {
             super(operation);
             if (operation.getParams().containsField(DataObjectCondition.QUERY_FILTER_PARAM)) {
                 filterCondition = DataObjectCondition.valueOf(operation.getParams().getField(DataObjectCondition.QUERY_FILTER_PARAM, Record.class));
             } else {
                 filterCondition = null;
             }
         }

         @Override
         public boolean processStep() throws Exception {
             Long maxID = operation.getParams().getField("ONE_BY_ONE", java.lang.Long.class, Long.MIN_VALUE);
             if (maxID > 0) {
                 processOneByOne(maxID);
             } else {
                 if (operation.fieldsToReturn().length == 1 && DataObject.ID_FIELD.equals(operation.fieldsToReturn()[0]) && filterCondition == null) {
                     super.addIDs(getStorage().keySet().iterator());
                 } else {
                     if (filterCondition == null) {
                         super.evaluate(getStorage().values().iterator());
                     } else {
                         super.evaluate(getStorage().values().stream().filter(filterCondition::matches).iterator());
                     }
                 }
             }
             successful = true;
             return false;
         }

         protected void processOneByOne(long maxID) throws IOException {
             Logger.getLogger(AbstractMapDBRefiner.class.getName()).log(Level.WARNING, "running the printAll operation one-by-one, up to: {0}", maxID);
             for (long id = 0; id <= maxID; id++) {
                 try {
                     DataObject obj = readObject(String.valueOf(id));
                     if (obj != null) {
                         super.print(obj);
                     }
                 } catch (java.lang.AssertionError ex) {
                     Logger.getLogger(AbstractMapDBRefiner.class.getName()).log(Level.WARNING, "MapDB trows AssertionError when reading object with id: {0}", id);
                 }
             }
         }

     }

    @Override
    public String toString() {
        return getName() + " storing " + getNumberOfObjects() + " objects";
    }

 }
