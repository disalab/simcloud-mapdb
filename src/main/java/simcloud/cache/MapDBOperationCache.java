/*
 *  This file is part of SimCloud MapDB library: https://bitbucket.org/disalab/simcloud-mapdb
 *
 *  SimCloudMapDB library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloudMapDB library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloudMapDB library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.cache;


import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;
import org.mapdb.DBMaker;
import org.mapdb.HTreeMap;

import java.io.IOException;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * NOT FINISHED, see the Ispn cache
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class MapDBOperationCache<TRequest extends AbstractOperation, TAnswer extends AbstractAnswer> extends OperationCache<TRequest, TAnswer>  {

    public static final double DEFAULT_CACHE_SIZE = 0.1d;

    protected transient HTreeMap<String, TAnswer> cache;

    public MapDBOperationCache(Class<TRequest> operationClass, Class<TAnswer> answerClass) throws IllegalArgumentException {
        super("Operation cache implemented by MapDB", operationClass, answerClass);
        cache = createCache();
    }

    public MapDBOperationCache(Class<TRequest> operationClass, Class<TAnswer> tAnswerClass, Predicate<TRequest> filter, Function<TRequest, String> paramGetter, long maxTime, int maxEntries) throws IllegalArgumentException {
        super("Operation cache implemented by MapDB", operationClass, tAnswerClass, filter, paramGetter, maxTime, maxEntries);
    }


    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        this.cache = createCache();
    }    
    
    private HTreeMap<String, TAnswer> createCache() {
        //Caches.LRU lru = new Caches.LRU(null, maxEntries, true);
        return DBMaker.newCacheDirect(DEFAULT_CACHE_SIZE);
    }


    @Override
    TAnswer getCachedAnswer(String operationString) {
        return cache.get(operationString);
    }

    @Override
    TAnswer putIfAbsent(String operationString, TAnswer answer) {
        return cache.putIfAbsent(operationString, answer);
    }
}
