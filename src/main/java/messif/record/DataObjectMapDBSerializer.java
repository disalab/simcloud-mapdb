/*
 *  This file is part of SimCloud MapDB library: https://bitbucket.org/disalab/simcloud-mapdb
 *
 *  SimCloudMapDB library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloudMapDB library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloudMapDB library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.record;

import messif.data.DataObject;
import org.mapdb.Serializer;
import org.mapdb.SerializerBase;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

/**
 * Implementation of the {@link Serializer} for MESSIF {@link DataObject}.
 *  types of stored objects are differentiated by a header byte.
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class DataObjectMapDBSerializer extends SerializerBase implements Serializable  {

    protected interface MessifHeader {
            int DATA_OBJECT_HEADER = 201;
    }
    
    @Override
    public void serialize(DataOutput out, Object value) throws IOException {        
        if (value instanceof DataObject) {
            serializeDataObject(out, (DataObject) value);
        } else {
            super.serialize(out, value);
        }
    }
    
    /**
     * Writes given LocalAbstractObject into the output; including the header byte depending on
     *  the type of the object.
     * @param out output of the MapDB 
     * @param object object to write into the output
     * @throws IOException if anything goes wrong
     */
    protected void serializeDataObject(DataOutput out, DataObject object) throws IOException {
        // write the header
        out.write(MessifHeader.DATA_OBJECT_HEADER);
        if (! (object instanceof RecordImpl)) {
            throw new IOException("MapDB cannot serialize object of this type: " + object.getClass());
        }
        // write the data
        super.serialize(out, ((RecordImpl) object).getDirectMap());
    }
    
    // ******************      Deserialization       **************************** //
    
    /**
     * This method should be called ONLY for the top object to deserialize, which should always be
     *  the LocalAbstractObject created by this serializer.
     * @param input
     * @param available
     * @return
     * @throws IOException 
     */
    @Override
    public DataObject deserialize(DataInput input, int available) throws IOException {
        final int head = input.readUnsignedByte();
        
        switch (head) {
            case MessifHeader.DATA_OBJECT_HEADER:
                return deserializeDataObject(input, available);
            default:
                throw new IOException("unknown type byte of the MESSIF MapDB serializer: " + head);
        }
    }

    protected DataObject deserializeDataObject(DataInput input, int available) throws IOException {
        return new RecordImpl((Map) super.deserialize(input, available), true);
    }

    @Override
    public int fixedSize() {
        return -1;
    }
}
